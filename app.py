#-*- coding=utf-8 -*-
from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
from hook import handle 
import json

def webhook(request):
    repositoryPath = '/opt/boltdog-front'
    print request.body
    handle(repositoryPath, request.body)
    resp = Response("")
    return resp

def config_route(config):
    config.add_route('webhook', '/')
    config.add_view(webhook, route_name='webhook')

if __name__ == '__main__':
    config = Configurator()
    config_route(config)
    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 7777, app)
    print "Listening on port 7777...."
    server.serve_forever()