#-*- coding=utf-8 -*-
import os
import shlex
import subprocess
import json
import exceptions

'''
克隆仓库
'''
def cloneRepository(path, repository):
    print '\n*****clone repository*****'
    initCmd = 'git init'
    pullCmd = 'git pull origin master'
    remoteCmd = 'git remote add origin %s' % repository
    submoduleCmd = 'git submodule update --init'
    chk = executeCommand(initCmd, path)
    if chk == 0:
        chk = executeCommand(remoteCmd, path)
    if chk == 0:
        chk = executeCommand(pullCmd, path)
    if chk == 0:
        chk = executeCommand(submoduleCmd, path)
    if chk == 0:
        updateSubmodule(path)
        updateNodeDependency(path)
        reloadWebpackDevServer(path)
    else:
        print 'repository init failure'

'''
更新仓库
'''
def updateRepository(path, updateDependency = False):
    print '\n*****update repository*****'
    cmd = 'git pull origin master'
    if executeCommand(cmd, path) == 0:
        updateSubmodule(path)
        #更新依赖
        if updateDependency:
            updateNodeDependency(path)
            reloadWebpackDevServer(path)
    else:
        raise exceptions.RepositoryExcetpion('update repository error')

'''
更新公共模块
'''
def updateSubmodule(path):
    print '\n*****update submodule*****'
    cmd = 'git pull origin master'
    if path[-1] == '/':
        path = path + 'public'
    else:
        path = path + '/public'

    if executeCommand(cmd, path) != 0:
        raise exceptions.RepositoryExcetpion('update submodule error')

'''
更新nodejs依赖
'''
def updateNodeDependency(path):
    print '\n*****install node dependencies*****'
    cmd = 'npm install'
    if executeCommand(cmd, path) != 0:
        raise exceptions.DependencyException('update node dependencies error')

'''
重启webpack开发服务器
'''
def reloadWebpackDevServer(path):
    print '\n*****reload webpack dev server*****'
    stopCmd = 'killall node'
    startCmd = 'npm run dev'
    print 'begin stop server'
    if executeCommand(stopCmd) == 0:
        print 'begin start server'
        if executeCommand(startCmd, path) == 0:
            print 'server start success'
        else:
            print 'server start error'
    else:
        print 'stop server error'

'''
检查package.json文件是否在提交中
'''
def checkPackageInCommits(commits):
    chk = False
    for commit in commits:
        files = commit['added'] + commit['modified']
        fileString = ','.join(files)
        chk = fileString.find('package.json') != -1
        if chk:
            break
    return chk

'''
执行命令
'''
def executeCommand(cmd, cwd=None, shell=False):
    if shell:
        cmdList = cmd
    else:
        cmdList = shlex.split(cmd)

    #没有指定标准输出和错误输出的管道，因此会打印到屏幕上；
    sub = subprocess.Popen(cmdList,
                           cwd=cwd,
                           stdin=subprocess.PIPE,
                           shell=shell,
                           bufsize=4096)

    #等待执行技术
    print "[%s] ecex..." % cmd
    sub.wait()
    return sub.returncode

'''
处理推送事件
'''
def handlePushEvent(eventInfo, repositoryPath):
    if os.path.isdir(repositoryPath):
        updateReponsitory(repositoryPath)
    else:
        cloneRepository(repositoryPath)

'''
转换推送数据
'''
def translatePushData(content):
    content = content.replace('\n', ' ').replace('\r', ' ')
    return json.loads(content)

'''
处理方法
'''
def handle(repositoryPath, content):
    try:
        data = translatePushData(content)
        isUpdateDependecy = checkPackageInCommits(data['commits'])
        if os.path.isdir(repositoryPath):
            updateRepository(repositoryPath, isUpdateDependecy)
        else:
            print "create dirs: %s" % repositoryPath
            os.makedirs(repositoryPath)
            cloneRepository(repositoryPath, data['repository']['git_ssh_url'])
    except ValueError, valueError:
        print 'json format error'